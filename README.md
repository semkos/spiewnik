# Śpiewnik Gitarowy

## Zależności (Fedora)

```bash
dnf install texlive-scheme-basic texlive-ucs texlive-polski
```

## Budowanie dokumentu

```bash
mkdir build
cd build
cmake ..
cmake --build .
```

Wyjściowy plik `spiewnik.pdf` zostanie zbudowany w katalogu `build`.
